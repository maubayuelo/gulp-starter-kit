# README #

### What is this repository for? ###

* A base for start projects using Gulp compiler
* Produce compiled SCSS and compressed CSS
* Produce compresed JS

### How do I get set up? ###

* If you are running this for first time, download repository and run `npm install`. It will install node_packages folder with dependencies
* Run `gulp` to get SCSS and JS compiled, to work locally and watch changes
* gulpfile.js is the file to edit in order to compress or not js or csss files (js: .pipe(uglify()), css: var browserSync)
* On gulpfile.js, on task serve. edit browserSync.init to preview changes using the root file or a local server

### Contribution guidelines ###

* This starter kit is based on LevelUpTuts series tutorial. https://youtu.be/wNlEK8qrb0M

### Who do I talk to? ###

* Feel free to download and use this starter kit