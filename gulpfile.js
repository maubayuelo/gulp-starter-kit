var gulp = require ('gulp');
var uglify = require ('gulp-uglify');
var sass = require('gulp-sass');
const { gulpSassError } = require('gulp-sass-error');
const throwError = true;
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var autoprefixerOptions = {
  browsers: ['last 2 versions', '> 5%', 'Firefox ESR']
};
var browserSync = require('browser-sync').create();
var sassOptions = {
  outputStyle: 'nested'
  //outputStyle: 'compact'
  //outputStyle: 'eÍxpanded'
  //outputStyle: 'compressed'
};

// BROWSERSYNC
// Static server
// Static Server + watching scss/html files
gulp.task('serve', function() {

    browserSync.init({
        server: "./"
    });
    // or...
    // browserSync.init({
    //    proxy: "yourlocal.dev"
    // });
    gulp.watch("source/js/**/*.js").on('change', browserSync.reload);
    gulp.watch("source/scss/**/*.scss").on('change', browserSync.reload);
    gulp.watch("./*.html").on('change', browserSync.reload);
});



// RUNNING SCRIPTS TASKS
gulp.task('compressVendorJS', function () {
  gulp.src('source/js/vendor/*.js')
  .pipe(uglify())
  .pipe(gulp.dest('dist/js/vendor'))
});
gulp.task('compressMainJS', function () {
  gulp.src('source/js/*.js')
  .pipe(uglify())
  .pipe(sourcemaps.write())
  .pipe(gulp.dest('dist/js'))
});

gulp.task('scripts', function () {
  gulp.start('compressVendorJS');
  gulp.start('compressMainJS');
});

// RUNNING SASS TASKS
gulp.task('mainSass', function () {
  return gulp.src('source/scss/main.scss')
    .pipe(sass(sassOptions).on('error', gulpSassError(throwError)))
    .pipe(sourcemaps.write())
    .pipe(autoprefixer(autoprefixerOptions))
    .pipe(gulp.dest('dist/css'));
});
gulp.task('vendorSass', function () {
  return gulp.src('source/scss/vendor/*.scss')
  .pipe(sass(sassOptions).on('error', gulpSassError(throwError)))
  .pipe(sourcemaps.write())
  .pipe(autoprefixer(autoprefixerOptions))
  .pipe(gulp.dest('dist/css/vendor'));
});

// RUNNING IMAGES COMPRESSOR
gulp.task('image', function () {
  gulp.src('source/images/*')
    .pipe(image({
      pngquant: true,
      optipng: false,
      zopflipng: true,
      jpegRecompress: false,
      jpegoptim: true,
      mozjpeg: true,
      guetzli: false,
      gifsicle: true,
      svgo: true,
      concurrent: 10
    }))
    .pipe(gulp.dest('dist/images'));
});


//WATCHER
gulp.task('watch', function () {
  gulp.watch('source/js/**/*.js', ['scripts']);
  gulp.watch('source/scss/**/*.scss', ['mainSass', 'vendorSass']);
  gulp.task('browser-sync');
});

// RUN MAIN TASK
gulp.task('default', ['scripts', 'mainSass', 'vendorSass', 'watch', 'serve'])
